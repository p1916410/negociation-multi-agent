import sys
import time

from PyQt5.QtWidgets import QApplication

from src.data.data import data_companies
from src.data.data import data_tickets
from src.ui.window import Window
from src.world.company import Company
from src.world.ticket import Ticket
from src.world.world import World

if __name__ == "__main__":

    tickets_list = []
    companies_list = []

    for ticket in data_tickets:
        tickets_list.append(
            Ticket(ticket["price"], ticket["date"], ticket["departure_city"], ticket["destination_city"],
                   ticket["destination_country"], ticket["destination_region"], ticket["departure_country"],
                   ticket["departure_region"], ticket["flight_number"]))
    i = 0
    for company in data_companies:
        companies_list.append(Company(company["name"], tickets_list[i:i + 9]))
        i += 10

    w = World(companies_list)

    while not w.is_finished:
        time.sleep(1)

    windows = []

    app = QApplication(sys.argv)

    # Crée une window par objet Négociation et l'affiche

    for key, negociation in w.negotiations.items():
        window = Window(negociation.agents)
        window.update_window(negociation.messages)
        windows.append(window)
    windows.reverse()

    for window in windows:
        window.show()

    sys.exit(app.exec())
