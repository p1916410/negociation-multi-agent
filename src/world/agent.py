import enum
import random
import threading as th
import time
from copy import deepcopy
from typing import Union

from src.world.message import Message, MessageType
from src.world.strategyBuyer import StrategyBuyer
from src.world.ticket import Ticket, ProposalTicket


class Status(enum.Enum):
    SUCCESS = "SUCCESS"
    FAIL = "FAIL"
    IN_PROGRESS = "IN PROGRESS"


class Agent(th.Thread):
    count = 1
    colors = ['#e6194baa', '#3cb44baa', '#ffe119aa', '#4363d8aa', '#f58231aa', '#911eb4aa', '#46f0f0aa', '#f032e6aa',
              '#bcf60caa', '#fabebeaa', '#008080aa', '#e6beffaa', '#9a6324aa', '#fffac8aa', '#800000aa', '#aaffc3aa',
              '#808000aa', '#ffd8b1aa', '#000075aa', '#808080aa', '#ffffff']

    def __init__(self, world):
        super(Agent, self).__init__()
        self.number = self.__class__.count
        self.__class__.count += 1

        self.color = random.choice(self.__class__.colors)

        self.world = world
        self.world.mailbox.add_agent(self.__repr__())
        self.mails: list[Message] = []

    def __del__(self):
        self.world.mailbox.remove_agent(self.__repr__())

    def update(self):
        pass

    def run(self) -> None:
        self.mails = self.world.mailbox.get_messages(self.__repr__())

    def decline(self, agent_hash: str, ticket, negotiation_id=None, reason=None):
        msg = Message(MessageType.DECLINE, self.__repr__(), ticket, agent_hash,
                      negotiation_id=negotiation_id,
                      reason=reason)
        self.world.mailbox.send_message(agent_hash, msg)
        self.world.negotiate(self.__repr__(), agent_hash, ticket, message=msg)

    def decline_already(self, agent_hash: str, ticket=None, negotiation_id=None):
        msg = Message(MessageType.ALREADY_NEGOTIATING, self.__repr__(), ticket, agent_hash,
                      negotiation_id=negotiation_id)
        self.world.mailbox.send_message(agent_hash, msg)
        self.world.negotiate(self.__repr__(), agent_hash, ticket, message=msg)

    def accept(self, agent_hash: str, ticket=None, negotiation_id=None):
        msg = Message(MessageType.ACCEPT, self.__repr__(), ticket, agent_hash,
                      negotiation_id=negotiation_id)
        self.world.mailbox.send_message(agent_hash, msg)
        self.world.negotiate(self.__repr__(), agent_hash, ticket, message=msg)


class Seller(Agent):
    def __init__(self, world, company, strategy):
        super(Seller, self).__init__(world)
        self.company = company
        self.strategy = strategy
        self.not_finish = True

    def __del__(self):
        super(Seller, self).__del__()

    def __repr__(self):
        return f"Seller n°{self.number}"

    def check_if(self, p_ticket) -> Union[bool, Ticket]:
        for ticket in self.company.tickets:
            if (p_ticket.departure_city == ticket.departure_city and
                    p_ticket.destination_city == ticket.destination_city and
                    p_ticket.date >= ticket.date):
                return ticket
        return False

    def remove_ticket(self, flight_number):
        for ticket in self.company.tickets:
            if ticket.flight_number == flight_number:
                self.company.tickets.remove(ticket)
                return

    def stop(self):
        self.not_finish = False

    def run(self) -> None:
        while self.not_finish:
            super(Seller, self).run()
            if len(self.mails):
                print("Seller : ", self.mails)
                for mail in self.mails:
                    self.world.mailbox.remove_message(self.__repr__(), mail)
                    if mail.message_type == MessageType.PROPOSE or mail.message_type == MessageType.COUNTER_OFFER:
                        res = deepcopy(self.check_if(mail.ticket))
                        if res:
                            if self.strategy.is_in_negotiation(res.flight_number, mail.sender):
                                self.decline_already(mail.sender, mail.ticket, mail.negotiation_id)
                            else:
                                is_viable, text_viable = self.strategy.viable_offer(
                                    res.flight_number, mail.ticket.price, mail.sender)
                                if is_viable:
                                    new_price, text_offer = self.strategy.propose_offer(res.flight_number)
                                    if not new_price:
                                        self.decline(mail.sender, mail.ticket, mail.negotiation_id, reason=text_offer)
                                        self.strategy.end_discussion(res.flight_number)
                                    elif new_price <= mail.ticket.price:
                                        self.accept(mail.sender, mail.ticket, mail.negotiation_id)
                                        self.remove_ticket(mail.ticket.flight_number)
                                        self.strategy.end_discussion(res.flight_number)
                                    else:
                                        res.price = new_price
                                        msg = Message(MessageType.COUNTER_OFFER, self.__repr__(), res,
                                                      negotiation_id=mail.negotiation_id, recipient=mail.sender,
                                                      company=self.company.name)
                                        self.world.negotiate(self.__repr__(), mail.sender, mail.negotiation_id,
                                                             new_price, msg)
                                        self.world.mailbox.send_message(
                                            mail.sender, msg)
                                else:
                                    self.decline(mail.sender, mail.ticket, negotiation_id=mail.negotiation_id,
                                                 reason=text_viable)
                                    self.strategy.end_discussion(res.flight_number)
                        else:
                            self.decline(mail.sender, mail.ticket, negotiation_id=mail.negotiation_id,
                                         reason="Pas de ticket correspondant.")
                    elif mail.message_type == MessageType.ACCEPT:
                        self.remove_ticket(mail.ticket.flight_number)
                        self.strategy.end_discussion(mail.ticket.flight_number)
                    elif mail.message_type == MessageType.DECLINE:
                        self.strategy.end_discussion(mail.ticket.flight_number)
            else:
                time.sleep(.2)


class Buyer(Agent):
    count_ids = 0

    def __init__(self, world, strategy: StrategyBuyer, dep_city: str, dest_city: str, max_date):
        super(Buyer, self).__init__(world)
        self.strategy = strategy
        self.dep_city = dep_city
        self.dest_city = dest_city
        self.max_date = max_date
        self.status = Status.IN_PROGRESS

        self.p_ticket = ProposalTicket(strategy.start_price, max_date, dep_city, dest_city)
        self.f_ticket = None

    def __del__(self):
        super(Buyer, self).__del__()

    def __repr__(self):
        return f"Buyer n°{self.number}"

    def loop(self, sellers: list[str]) -> (bool, list[str]):
        remaining = []
        for s in sellers:
            msg = Message(MessageType.PROPOSE, self.__repr__(), self.p_ticket, recipient=s,
                          negotiation_id=self.__class__.count_ids)
            self.__class__.count_ids += 1
            self.world.mailbox.send_message(s, msg)
            self.world.negotiate(self.__repr__(), s, msg.negotiation_id, self.p_ticket.price, msg)
            state = MessageType.PROPOSE
            while (state != MessageType.ACCEPT and state != MessageType.DECLINE and
                   state != MessageType.ALREADY_NEGOTIATING):
                super(Buyer, self).run()
                if self.mails:
                    print("Buyer : ", self.mails)
                    mail = self.mails[0]
                    state = mail.message_type
                    if state == MessageType.ACCEPT:
                        self.f_ticket = mail.ticket
                    elif state == MessageType.COUNTER_OFFER:
                        self.p_ticket.flight_number = mail.ticket.flight_number
                        is_viable, text_viable = self.strategy.viable_offer(mail.ticket.flight_number,
                                                                            mail.ticket.price, s, mail.company)
                        if is_viable:
                            new_price, text_offer = self.strategy.propose_offer(mail.ticket.flight_number, mail.company)
                            if not new_price:
                                state = MessageType.DECLINE
                                self.decline(s, mail.ticket, negotiation_id=mail.negotiation_id, reason=text_offer)
                            elif new_price >= mail.ticket.price:
                                self.accept(s, ticket=self.p_ticket, negotiation_id=mail.negotiation_id)
                                state = MessageType.ACCEPT
                                self.f_ticket = mail.ticket
                            else:
                                self.p_ticket.price = new_price
                                msg = Message(MessageType.COUNTER_OFFER,
                                              self.__repr__(), self.p_ticket,
                                              negotiation_id=mail.negotiation_id,
                                              recipient=s)
                                self.world.negotiate(self.__repr__(), s, self.p_ticket.flight_number, new_price, msg)
                                self.world.mailbox.send_message(s, msg)
                        else:
                            state = MessageType.DECLINE
                            self.decline(s, mail.ticket, negotiation_id=mail.negotiation_id, reason=text_viable)
                    elif state == MessageType.ALREADY_NEGOTIATING:
                        remaining.append(s)
                    self.world.mailbox.remove_message(self.__repr__(), mail)
                else:
                    time.sleep(.2)

            if state == MessageType.ACCEPT:
                return True, []
        return False, remaining

    def run(self) -> None:
        super(Buyer, self).run()

        success, remaining = False, self.world.get_all_sellers()
        while len(remaining) and not success:
            success, remaining = self.loop(remaining)

        if self.f_ticket:
            print("Buyer success ! : ", self.f_ticket,
                  "\nBudget: ", self.strategy.max_price,
                  ", Started at: ", self.strategy.start_price, ", Got it for: ", self.f_ticket.price)
            self.status = Status.SUCCESS
        else:
            print("Buyer fail ! : ",
                  "\nBudget: ", self.strategy.max_price,
                  ", Started at: ", self.strategy.start_price)

            self.status = Status.FAIL
        self.world.notify_buyer_complete(self)
