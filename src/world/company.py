class Company:
    def __init__(self, name: str, tickets=None):
        self.name = name
        self.tickets = [] if tickets is None else tickets

    def __str__(self):
        return self.name
