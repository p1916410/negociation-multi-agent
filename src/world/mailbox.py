import copy
from typing import List, Dict

from src.world.message import Message


class Mailbox:
    def __init__(self):
        self.messages: Dict[int, List[Message]] = {}
        self.messages_history = []

    def add_agent(self, agent_hash: int) -> None:
        self.messages[agent_hash] = []

    def remove_agent(self, agent_hash: int) -> None:
        self.messages.pop(agent_hash)

    def get_messages(self, agent_hash: int) -> List[Message]:
        return self.messages[agent_hash]

    def send_message(self, agent_hash: int, message: Message) -> None:
        self.messages[agent_hash].append(message)
        if len(self.messages_history) >= 420:
            self.messages_history.pop(0)
        self.messages_history.append(copy.deepcopy(message))

    def remove_message(self, agent_hash: int, message: Message) -> None:
        self.messages[agent_hash].remove(message)
