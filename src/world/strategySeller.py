import random as r
from typing import Callable, Union

from src.world.evolutionFunctions import *
from src.world.ticket import Ticket


class StrategySeller:
    def __init__(self, tickets: list[Ticket], frequency: int = 99,
                 start_price_f: Callable[[int], int] = random_50_100,
                 min_decrease: Callable[[int], int] = decrease_5p,
                 max_decrease: Callable[[int], int] = decrease_10p,
                 min_increase: Callable[[int], int] = increase_4p):

        self.tickets = tickets
        self.frequency = frequency
        self.start_price_f = start_price_f
        self.max_decrease = max_decrease
        self.min_decrease = min_decrease
        self.min_increase = min_increase
        self.nb: dict[str, int] = {}
        self.ref: dict[str, str] = {}
        self.offers: dict[str, list[int]] = {}  # Receive
        self.demands: dict[str, list[int]] = {}  # Send

    def is_in_negotiation(self, flight_number: str, agent_name: str):
        if flight_number in self.ref:
            return self.ref[flight_number] != agent_name
        return False

    def get_ticket_price(self, flight_number: str):
        for ticket in self.tickets:
            if ticket.flight_number == flight_number:
                return ticket.price

    def viable_offer(self, flight_number: str, offer: int, agent_name: str) -> (bool, str):
        if flight_number in self.offers and offer < self.min_increase(self.offers[flight_number][-1]):
            return False, "Acheteur n'a pas assez augmenté."

        if flight_number not in self.offers:
            self.ref[flight_number] = agent_name
            self.offers[flight_number] = []
            self.nb[flight_number] = 0
        self.offers[flight_number].append(offer)
        return True, ""

    def propose_offer(self, flight_number: str) -> (Union[int, bool], str):
        if self.nb[flight_number] >= self.frequency:
            return False, "Trop de demandes."
        self.nb[flight_number] += 1

        if flight_number not in self.demands:
            start_price = self.start_price_f(self.get_ticket_price(flight_number))
            self.demands[flight_number] = [start_price]
            return start_price, ""

        last_demand = self.demands[flight_number][-1]
        min_demand = int(self.min_decrease(last_demand))
        max_demand = int(max(self.max_decrease(last_demand), self.get_ticket_price(flight_number)))

        if min_demand <= max_demand:
            return False, "Prix trop bas, pas d'accord."

        new_offer = r.randint(max_demand, min_demand)
        self.demands[flight_number].append(new_offer)
        return new_offer, ""

    def end_discussion(self, flight_number: str):
        self.offers.pop(flight_number)
        self.demands.pop(flight_number)
        self.nb.pop(flight_number)
