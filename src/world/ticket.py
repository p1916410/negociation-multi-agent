import datetime


class Ticket:
    def __init__(self, price: int, date: datetime.datetime, departure_city, destination_city,
                 destination_country=None, destination_region=None, departure_country=None, departure_region=None,
                 flight_number=None):
        self.price = price
        self.date = date
        self.destination_country = destination_country
        self.destination_region = destination_region
        self.destination_city = destination_city
        self.departure_country = departure_country
        self.departure_region = departure_region
        self.departure_city = departure_city
        self.flight_number = flight_number

    def __str__(self):
        return f"Ticket N°{self.flight_number}"

    def __repr__(self):
        return self.__str__()


class ProposalTicket(Ticket):
    pass
