import random


def increase_5p(precedent: int):
    return precedent * 1.05


def increase_4p(precedent: int):
    return precedent * 1.04


def increase_3p(precedent: int):
    return precedent * 1.03


def increase_10p(precedent: int):
    return precedent * 1.1


def decrease_5p(precedent: int):
    return precedent * .95


def decrease_4p(precedent: int):
    return precedent * .96


def decrease_3p(precedent: int):
    return precedent * .97


def decrease_10p(precedent: int):
    return precedent * .90


def random_50_100(price: int):
    return round(price * random.uniform(1.5, 2.5))
