import datetime
import enum


class MessageType(enum.Enum):
    ACCEPT = "Accept"
    DECLINE = "Decline"
    ALREADY_NEGOTIATING = "Already negotiating"
    PROPOSE = "Propose"
    COUNTER_OFFER = "Counter Offer"


class Message:
    def __init__(self, m_type: MessageType, sender: str, ticket=None,
                 recipient=None, negotiation_id=None, reason="", company: str = None):
        self.sender = sender
        self.recipient = recipient
        self.timestamp = datetime.datetime.now()
        self.message_type = m_type
        self.ticket = ticket
        self.negotiation_id = negotiation_id
        self.reason = reason
        self.company = company

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"{self.message_type} -{self.reason}- {self.ticket}"
