import json
from copy import deepcopy
from datetime import datetime as dt

from src.world.agent import Seller, Buyer
from src.world.mailbox import Mailbox
from src.world.negotiation import Negotiation
from src.world.strategyBuyer import StrategyBuyer
from src.world.strategySeller import StrategySeller


class World:
    def __init__(self, companies_list):

        self.mailbox = Mailbox()

        self.sellers: list[Seller] = []
        self.buyers: list[Buyer] = []
        self.negotiations: dict[tuple, Negotiation] = {}
        self.companies_list = companies_list

        self.is_finished = False
        for company in companies_list:
            self.sellers.append(Seller(self, company, StrategySeller(company.tickets, 99)))

        with open("src/buyers.json") as f:
            buyers = json.load(f)["buyers"]

        for buyer in buyers:
            self.buyers.append(
                Buyer(self, StrategyBuyer(buyer["start_price"], buyer["max_price"], buyer["frequency"],
                                          buyer["unwanted_companies"], buyer["preferred_companies"]),
                      buyer["departure_city"], buyer["destination_city"], dt.fromisoformat(buyer["max_date"])))
            # self.buyers.append(
            #     Buyer(self, StrategyBuyer(300, 1000, 99), "Tolyatti", "Dublin", datetime.now() + timedelta(days=3000),
            #           2000)
            # )
            #
            # self.buyers.append(
            #     Buyer(self, StrategyBuyer(300, 1000, 99), "Bogotá", "Falun", datetime.now() + timedelta(days=3000),
            #           2000)
            # )

        self.start_threads()

    def negotiate(self, agent1, agent2, service, offer=None, message=None):

        agent1 = [agent for agent in self.sellers + self.buyers if agent.__repr__() == agent1]
        agent2 = [agent for agent in self.sellers + self.buyers if agent.__repr__() == agent2]

        key = tuple(sorted((agent1.__repr__(), agent2.__repr__())))

        if key not in self.negotiations.keys():
            self.negotiations[key] = Negotiation(service, [agent1[0], agent2[0]])

        if offer:
            self.negotiations[key].add_offer(agent1.__repr__(), offer)

        self.negotiations[key].add_message(deepcopy(message))

    def get_all_buyers(self) -> list[str]:
        return [b.__repr__() for b in self.buyers]

    def get_all_sellers(self) -> list[str]:
        return [s.__repr__() for s in self.sellers]

    def start_threads(self):
        for s in self.sellers:
            s.start()
        for b in self.buyers:
            b.start()

    def notify_buyer_complete(self, buyer) -> None:
        self.buyers.remove(buyer)
        if not len(self.buyers):
            print("All buyers finished ! Stopping sellers.")
            for s in self.sellers:
                s.stop()
            print(self.negotiations)
            self.is_finished = True

    def update(self):
        pass
