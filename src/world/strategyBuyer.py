import random as r
from typing import Callable, Union

from src.world.evolutionFunctions import *


class StrategyBuyer:
    def __init__(self, start_price: int, max_price: int, frequency: int = 99,
                 unwanted_companies: list[str] = None,
                 preferred_companies: list[str] = None,
                 min_increase: Callable[[int], int] = increase_5p,
                 max_increase: Callable[[int], int] = increase_10p,
                 min_decrease: Callable[[int], int] = decrease_4p):

        self.start_price = start_price
        self.max_price = max_price
        self.frequency = frequency
        self.unwanted_companies = [] if unwanted_companies is None else unwanted_companies
        self.preferred_companies = [] if preferred_companies is None else preferred_companies
        self.max_increase = max_increase
        self.min_increase = min_increase
        self.min_decrease = min_decrease
        self.nb: dict[str, int] = {}
        self.ref: dict[str, str] = {}
        self.offers: dict[str, list[int]] = {}  # Receive
        self.demands: dict[str, list[int]] = {}  # Send

    def viable_offer(self, flight_number: str, offer: int, agent_name: str, company_name: str = None) -> (bool, str):
        if company_name is not None and company_name in self.unwanted_companies:
            return False, "Unwanted company"

        if flight_number in self.offers and offer > self.min_decrease(self.offers[flight_number][-1]):
            return False, "Vendeur n'a pas assez diminué."

        if flight_number not in self.offers:
            self.ref[flight_number] = agent_name
            self.offers[flight_number] = []
            self.nb[flight_number] = 0
        self.offers[flight_number].append(offer)
        return True, ""

    def propose_offer(self, flight_number: str, company_name: str = None) -> (Union[int, bool], str):
        if self.nb[flight_number] >= self.frequency:
            return False, "Trop de demandes."
        self.nb[flight_number] += 1

        if flight_number not in self.demands:
            self.demands[flight_number] = [self.start_price]

        last_demand = self.demands[flight_number][-1]
        min_demand = int(self.min_increase(last_demand))
        max_demand = int(min(self.max_increase(last_demand), self.max_price))

        if (company_name is not None and company_name in self.preferred_companies and
                self.offers[flight_number][-1] <= self.max_price):
            return self.offers[flight_number][-1], ""

        if min_demand >= max_demand:
            return False, "Prix trop haut, pas d'accord."

        new_offer = r.randint(min_demand, max_demand)
        self.demands[flight_number].append(new_offer)
        return new_offer, ""

    def end_discussion(self, flight_number: str):
        self.offers.pop(flight_number)
        self.demands.pop(flight_number)
        self.nb.pop(flight_number)
        self.ref.pop(flight_number)
