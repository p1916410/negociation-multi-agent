from copy import deepcopy
from datetime import datetime

from src.world.agent import Agent


class Negotiation:

    def __init__(self, service: str, agents: list[Agent] = None):
        self.service = service
        self.agents = agents
        self.agents_repr = [agent.__repr__() for agent in agents]
        self.offers: dict[str, list] = {}
        self.messages = []

    def add_offer(self, agent, offer):
        self.add_agent(agent)
        self.offers[agent].append((deepcopy(offer), datetime.now().strftime("%d/%m/%Y %H:%M:%S.%f")))

    def add_agent(self, agent):
        if agent not in self.agents_repr:
            self.agents_repr.append(agent)
        if agent not in self.offers:
            self.offers[agent] = []

    def add_message(self, message):
        self.messages.append(message)

    def __contains__(self, item):
        if isinstance(item, tuple) and len(item) == 3:
            return self.service == item[0] and item[1] in self.agents_repr and item[2] in self.agents_repr
        return False

    def __str__(self):
        return f"Negotiation: {self.agents_repr}, {self.offers}"

    def __repr__(self):
        return self.__str__()
