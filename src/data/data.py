import datetime as dt

data_companies = [
    {
        "name": "Ut Tincidunt LLC"
    },
    {
        "name": "Lorem Ut Incorporated"
    },
    {
        "name": "Magnis Dis Parturient Associates"
    },
    {
        "name": "Donec Incorporated"
    },
    {
        "name": "Eget Mollis Lectus Inc."
    },
    {
        "name": "Faucibus LLP"
    },
    {
        "name": "Fusce Mi LLC"
    },
    {
        "name": "Dictum PC"
    },
    {
        "name": "Egestas Industries"
    },
    {
        "name": "Justo Proin Non Limited"
    }
]

data_tickets = [
    {
        "date": dt.datetime.fromisoformat("2021-03-16 02:03:00"),
        "destination_city": "Evere",
        "departure_city": "Kaluga",
        "price": 274,
        "departure_country": "Russian Federation",
        "departure_region": "Kaluga Oblast",
        "destination_country": "Belgium",
        "destination_region": "Brussels Hoofdstedelijk Gewest",
        "flight_number": "HB26542"
    },
    {
        "date": dt.datetime.fromisoformat("2023-05-13 17:05:00"),
        "destination_city": "Parral",
        "departure_city": "South Perth",
        "price": 730,
        "departure_country": "Australia",
        "departure_region": "Western Australia",
        "destination_country": "Chile",
        "destination_region": "Maule",
        "flight_number": "BT83261"
    },
    {
        "date": dt.datetime.fromisoformat("2023-10-19 00:10:00"),
        "destination_city": "Valladolid",
        "departure_city": "Cusco",
        "price": 461,
        "departure_country": "Peru",
        "departure_region": "Cusco",
        "destination_country": "Spain",
        "destination_region": "Castilla y León",
        "flight_number": "YY36842"
    },
    {
        "date": dt.datetime.fromisoformat("2021-06-02 04:06:00"),
        "destination_city": "San Marcos",
        "departure_city": "Dordrecht",
        "price": 485,
        "departure_country": "Netherlands",
        "departure_region": "Zuid Holland",
        "destination_country": "Colombia",
        "destination_region": "Sucre",
        "flight_number": "FP55873"
    },
    {
        "date": dt.datetime.fromisoformat("2023-08-18 02:08:00"),
        "destination_city": "Christchurch",
        "departure_city": "Palmerston",
        "price": 123,
        "departure_country": "Australia",
        "departure_region": "Northern Territory",
        "destination_country": "New Zealand",
        "destination_region": "South Island",
        "flight_number": "XQ16412"
    },
    {
        "date": dt.datetime.fromisoformat("2021-03-16 00:03:00"),
        "destination_city": "Yurimaguas",
        "departure_city": "Xalapa",
        "price": 262,
        "departure_country": "Mexico",
        "departure_region": "Veracruz",
        "destination_country": "Peru",
        "destination_region": "Loreto",
        "flight_number": "LN83888"
    },
    {
        "date": dt.datetime.fromisoformat("2021-09-26 05:09:00"),
        "destination_city": "Nocera Umbra",
        "departure_city": "Avesta",
        "price": 216,
        "departure_country": "Sweden",
        "departure_region": "Dalarnas län",
        "destination_country": "Italy",
        "destination_region": "Umbria",
        "flight_number": "YL56733"
    },
    {
        "date": dt.datetime.fromisoformat("2023-06-05 19:06:00"),
        "destination_city": "Salamanca",
        "departure_city": "Brive-la-Gaillarde",
        "price": 723,
        "departure_country": "France",
        "departure_region": "Limousin",
        "destination_country": "Mexico",
        "destination_region": "Guanajuato",
        "flight_number": "OC27274"
    },
    {
        "date": dt.datetime.fromisoformat("2021-09-01 15:09:00"),
        "destination_city": "Ceuta",
        "departure_city": "Tver",
        "price": 62,
        "departure_country": "Russian Federation",
        "departure_region": "Tver Oblast",
        "destination_country": "Spain",
        "destination_region": "Ceuta",
        "flight_number": "GU18863"
    },
    {
        "date": dt.datetime.fromisoformat("2023-05-11 13:05:00"),
        "destination_city": "Guaitecas",
        "departure_city": "Villahermosa",
        "price": 759,
        "departure_country": "Mexico",
        "departure_region": "Tabasco",
        "destination_country": "Chile",
        "destination_region": "Aisén",
        "flight_number": "AK33327"
    },
    {
        "date": dt.datetime.fromisoformat("2021-01-15 22:01:00"),
        "destination_city": "Narbonne",
        "departure_city": "Harnai",
        "price": 828,
        "departure_country": "Pakistan",
        "departure_region": "Balochistan",
        "destination_country": "France",
        "destination_region": "Languedoc-Roussillon",
        "flight_number": "DE21666"
    },
    {
        "date": dt.datetime.fromisoformat("2022-08-26 22:08:00"),
        "destination_city": "Zona Bananera",
        "departure_city": "Flushing",
        "price": 76,
        "departure_country": "Netherlands",
        "departure_region": "Zeeland",
        "destination_country": "Colombia",
        "destination_region": "Magdalena",
        "flight_number": "ZD85945"
    },
    {
        "date": dt.datetime.fromisoformat("2022-12-12 11:12:00"),
        "destination_city": "Salzburg",
        "departure_city": "Tuyên Quang",
        "price": 130,
        "departure_country": "Vietnam",
        "departure_region": "Tuyên Quang",
        "destination_country": "Austria",
        "destination_region": "Salzburg",
        "flight_number": "QT25875"
    },
    {
        "date": dt.datetime.fromisoformat("2022-07-08 06:07:00"),
        "destination_city": "Galway",
        "departure_city": "Waitakere",
        "price": 284,
        "departure_country": "New Zealand",
        "departure_region": "North Island",
        "destination_country": "Ireland",
        "destination_region": "Connacht",
        "flight_number": "LB75652"
    },
    {
        "date": dt.datetime.fromisoformat("2021-05-12 13:05:00"),
        "destination_city": "Stockholm",
        "departure_city": "Castres",
        "price": 752,
        "departure_country": "France",
        "departure_region": "Midi-Pyrénées",
        "destination_country": "Sweden",
        "destination_region": "Stockholms län",
        "flight_number": "NY72747"
    },
    {
        "date": dt.datetime.fromisoformat("2020-12-10 23:12:00"),
        "destination_city": "Mohmand Agency",
        "departure_city": "Bloomington",
        "price": 251,
        "departure_country": "United States",
        "departure_region": "Minnesota",
        "destination_country": "Pakistan",
        "destination_region": "FATA",
        "flight_number": "NR35254"
    },
    {
        "date": dt.datetime.fromisoformat("2021-04-11 12:04:00"),
        "destination_city": "La Hulpe",
        "departure_city": "Haridwar",
        "price": 462,
        "departure_country": "India",
        "departure_region": "Uttarakhand",
        "destination_country": "Belgium",
        "destination_region": "Waals-Brabant",
        "flight_number": "UO24143"
    },
    {
        "date": dt.datetime.fromisoformat("2021-10-10 19:10:00"),
        "destination_city": "Yeosu",
        "departure_city": "Invercargill",
        "price": 970,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "South Korea",
        "destination_region": "South Jeolla",
        "flight_number": "LU53437"
    },
    {
        "date": dt.datetime.fromisoformat("2023-03-05 05:03:00"),
        "destination_city": "Tilburg",
        "departure_city": "Khyber Agency",
        "price": 504,
        "departure_country": "Pakistan",
        "departure_region": "FATA",
        "destination_country": "Netherlands",
        "destination_region": "Noord Brabant",
        "flight_number": "HF18311"
    },
    {
        "date": dt.datetime.fromisoformat("2023-08-04 21:08:00"),
        "destination_city": "Queenstown",
        "departure_city": "Hidalgo del Parral",
        "price": 90,
        "departure_country": "Mexico",
        "departure_region": "Chihuahua",
        "destination_country": "New Zealand",
        "destination_region": "South Island",
        "flight_number": "SI98777"
    },
    {
        "date": dt.datetime.fromisoformat("2022-06-26 08:06:00"),
        "destination_city": "Beerse",
        "departure_city": "Elbląg",
        "price": 302,
        "departure_country": "Poland",
        "departure_region": "Warmińsko-mazurskie",
        "destination_country": "Belgium",
        "destination_region": "Antwerpen",
        "flight_number": "IM62263"
    },
    {
        "date": dt.datetime.fromisoformat("2023-11-22 13:11:00"),
        "destination_city": "Sagamu",
        "departure_city": "Picton",
        "price": 984,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "Nigeria",
        "destination_region": "Ogun",
        "flight_number": "RP75952"
    },
    {
        "date": dt.datetime.fromisoformat("2022-04-22 20:04:00"),
        "destination_city": "Zhob",
        "departure_city": "Girona",
        "price": 800,
        "departure_country": "Spain",
        "departure_region": "Catalunya",
        "destination_country": "Pakistan",
        "destination_region": "Balochistan",
        "flight_number": "QS85982"
    },
    {
        "date": dt.datetime.fromisoformat("2023-06-22 21:06:00"),
        "destination_city": "Chungju",
        "departure_city": "Göksun",
        "price": 256,
        "departure_country": "Turkey",
        "departure_region": "Kahramanmaraş",
        "destination_country": "South Korea",
        "destination_region": "North Chungcheong",
        "flight_number": "KD27276"
    },
    {
        "date": dt.datetime.fromisoformat("2023-02-16 13:02:00"),
        "destination_city": "Aix-en-Provence",
        "departure_city": "Torrevieja",
        "price": 487,
        "departure_country": "Spain",
        "departure_region": "Comunitat Valenciana",
        "destination_country": "France",
        "destination_region": "Provence-Alpes-Côte d'Azur",
        "flight_number": "JW48868"
    },
    {
        "date": dt.datetime.fromisoformat("2022-03-16 10:03:00"),
        "destination_city": "Novgorod",
        "departure_city": "Tomsk",
        "price": 553,
        "departure_country": "Russian Federation",
        "departure_region": "Tomsk Oblast",
        "destination_country": "Russian Federation",
        "destination_region": "Novgorod Oblast",
        "flight_number": "NT47623"
    },
    {
        "date": dt.datetime.fromisoformat("2022-10-20 00:10:00"),
        "destination_city": "Burnie",
        "departure_city": "Oamaru",
        "price": 294,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "Australia",
        "destination_region": "Tasmania",
        "flight_number": "MQ64537"
    },
    {
        "date": dt.datetime.fromisoformat("2023-06-20 03:06:00"),
        "destination_city": "Ponoka",
        "departure_city": "Yên Bái",
        "price": 256,
        "departure_country": "Vietnam",
        "departure_region": "Yên Bái",
        "destination_country": "Canada",
        "destination_region": "Alberta",
        "flight_number": "VM26271"
    },
    {
        "date": dt.datetime.fromisoformat("2022-08-02 11:08:00"),
        "destination_city": "Delft",
        "departure_city": "Valuyki",
        "price": 977,
        "departure_country": "Russian Federation",
        "departure_region": "Belgorod Oblast",
        "destination_country": "Netherlands",
        "destination_region": "Zuid Holland",
        "flight_number": "XM96739"
    },
    {
        "date": dt.datetime.fromisoformat("2021-08-17 05:08:00"),
        "destination_city": "Hồ Chí Minh City",
        "departure_city": "South Jakarta",
        "price": 569,
        "departure_country": "Indonesia",
        "departure_region": "Special Capital Region of Jakarta",
        "destination_country": "Vietnam",
        "destination_region": "Hồ Chí Minh City",
        "flight_number": "NQ75364"
    },
    {
        "date": dt.datetime.fromisoformat("2022-07-14 10:07:00"),
        "destination_city": "Eastbourne",
        "departure_city": "Balclutha",
        "price": 93,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "United Kingdom",
        "destination_region": "Sussex",
        "flight_number": "HY24583"
    },
    {
        "date": dt.datetime.fromisoformat("2022-06-02 05:06:00"),
        "destination_city": "Bad Nauheim",
        "departure_city": "Ghotki",
        "price": 360,
        "departure_country": "Pakistan",
        "departure_region": "Sindh",
        "destination_country": "Germany",
        "destination_region": "Hessen",
        "flight_number": "XT65481"
    },
    {
        "date": dt.datetime.fromisoformat("2021-05-12 21:05:00"),
        "destination_city": "Bo'ness",
        "departure_city": "Galway",
        "price": 982,
        "departure_country": "Ireland",
        "departure_region": "Connacht",
        "destination_country": "United Kingdom",
        "destination_region": "West Lothian",
        "flight_number": "OK16569"
    },
    {
        "date": dt.datetime.fromisoformat("2022-11-13 00:11:00"),
        "destination_city": "Dublin",
        "departure_city": "Tolyatti",
        "price": 784,
        "departure_country": "Russian Federation",
        "departure_region": "Samara Oblast",
        "destination_country": "Ireland",
        "destination_region": "Leinster",
        "flight_number": "QJ23759"
    },
    {
        "date": dt.datetime.fromisoformat("2022-02-27 13:02:00"),
        "destination_city": "Coevorden",
        "departure_city": "Gore",
        "price": 889,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "Netherlands",
        "destination_region": "Drenthe",
        "flight_number": "UH37355"
    },
    {
        "date": dt.datetime.fromisoformat("2021-08-20 00:08:00"),
        "destination_city": "Darwin",
        "departure_city": "San Pedro",
        "price": 650,
        "departure_country": "Chile",
        "departure_region": "Metropolitana de Santiago",
        "destination_country": "Australia",
        "destination_region": "Northern Territory",
        "flight_number": "UJ79573"
    },
    {
        "date": dt.datetime.fromisoformat("2021-09-16 13:09:00"),
        "destination_city": "Dereham",
        "departure_city": "Hazaribag",
        "price": 305,
        "departure_country": "India",
        "departure_region": "Jharkhand",
        "destination_country": "United Kingdom",
        "destination_region": "Norfolk",
        "flight_number": "ZN47252"
    },
    {
        "date": dt.datetime.fromisoformat("2022-08-17 23:08:00"),
        "destination_city": "Värnamo",
        "departure_city": "San Andrés",
        "price": 112,
        "departure_country": "Colombia",
        "departure_region": "San Andrés y Providencia",
        "destination_country": "Sweden",
        "destination_region": "Jönköpings län",
        "flight_number": "FV21833"
    },
    {
        "date": dt.datetime.fromisoformat("2022-02-25 06:02:00"),
        "destination_city": "Rionero in Vulture",
        "departure_city": "Konin",
        "price": 681,
        "departure_country": "Poland",
        "departure_region": "Wielkopolskie",
        "destination_country": "Italy",
        "destination_region": "Basilicata",
        "flight_number": "ID48252"
    },
    {
        "date": dt.datetime.fromisoformat("2021-10-22 22:10:00"),
        "destination_city": "Lombardsijde",
        "departure_city": "Norrköping",
        "price": 699,
        "departure_country": "Sweden",
        "departure_region": "Östergötlands län",
        "destination_country": "Belgium",
        "destination_region": "West-Vlaanderen",
        "flight_number": "CD44286"
    },
    {
        "date": dt.datetime.fromisoformat("2022-10-16 13:10:00"),
        "destination_city": "Itajaí",
        "departure_city": "San Juan del Cesar",
        "price": 181,
        "departure_country": "Colombia",
        "departure_region": "La Guajira",
        "destination_country": "Brazil",
        "destination_region": "Santa Catarina",
        "flight_number": "ED85616"
    },
    {
        "date": dt.datetime.fromisoformat("2021-01-11 15:01:00"),
        "destination_city": "Crato",
        "departure_city": "Cuernavaca",
        "price": 631,
        "departure_country": "Mexico",
        "departure_region": "Morelos",
        "destination_country": "Brazil",
        "destination_region": "Ceará",
        "flight_number": "UI33525"
    },
    {
        "date": dt.datetime.fromisoformat("2022-04-17 13:04:00"),
        "destination_city": "Shaki",
        "departure_city": "Wichita",
        "price": 933,
        "departure_country": "United States",
        "departure_region": "Kansas",
        "destination_country": "Nigeria",
        "destination_region": "Oyo",
        "flight_number": "NG48386"
    },
    {
        "date": dt.datetime.fromisoformat("2022-08-08 08:08:00"),
        "destination_city": "Singkawang",
        "departure_city": "Erpion",
        "price": 692,
        "departure_country": "Belgium",
        "departure_region": "Henegouwen",
        "destination_country": "Indonesia",
        "destination_region": "West Kalimantan",
        "flight_number": "XH64495"
    },
    {
        "date": dt.datetime.fromisoformat("2022-09-14 13:09:00"),
        "destination_city": "Lolol",
        "departure_city": "Cajamarca",
        "price": 400,
        "departure_country": "Peru",
        "departure_region": "Cajamarca",
        "destination_country": "Chile",
        "destination_region": "O'Higgins",
        "flight_number": "OC87282"
    },
    {
        "date": dt.datetime.fromisoformat("2022-05-13 07:05:00"),
        "destination_city": "West Jakarta",
        "departure_city": "Opole",
        "price": 191,
        "departure_country": "Poland",
        "departure_region": "Opolskie",
        "destination_country": "Indonesia",
        "destination_region": "Special Capital Region of Jakarta",
        "flight_number": "JK85562"
    },
    {
        "date": dt.datetime.fromisoformat("2022-09-16 05:09:00"),
        "destination_city": "Da Lat",
        "departure_city": "Piedras Negras",
        "price": 815,
        "departure_country": "Mexico",
        "departure_region": "Coahuila",
        "destination_country": "Vietnam",
        "destination_region": "Lâm Đồng",
        "flight_number": "FH37172"
    },
    {
        "date": dt.datetime.fromisoformat("2023-08-08 12:08:00"),
        "destination_city": "Jammu",
        "departure_city": "Halberstadt",
        "price": 363,
        "departure_country": "Germany",
        "departure_region": "Sachsen-Anhalt",
        "destination_country": "India",
        "destination_region": "Jammu and Kashmir",
        "flight_number": "CS89752"
    },
    {
        "date": dt.datetime.fromisoformat("2023-03-02 05:03:00"),
        "destination_city": "North Las Vegas",
        "departure_city": "Chile Chico",
        "price": 627,
        "departure_country": "Chile",
        "departure_region": "Aisén",
        "destination_country": "United States",
        "destination_region": "Nevada",
        "flight_number": "BC26432"
    },
    {
        "date": dt.datetime.fromisoformat("2021-10-07 15:10:00"),
        "destination_city": "Flushing",
        "departure_city": "Huancayo",
        "price": 360,
        "departure_country": "Peru",
        "departure_region": "Junín",
        "destination_country": "Netherlands",
        "destination_region": "Zeeland",
        "flight_number": "RY65967"
    },
    {
        "date": dt.datetime.fromisoformat("2023-08-11 23:08:00"),
        "destination_city": "Newport News",
        "departure_city": "Istanbul",
        "price": 394,
        "departure_country": "Turkey",
        "departure_region": "Istanbul",
        "destination_country": "United States",
        "destination_region": "Virginia",
        "flight_number": "KZ62226"
    },
    {
        "date": dt.datetime.fromisoformat("2021-10-13 19:10:00"),
        "destination_city": "Whitehorse",
        "departure_city": "Avesta",
        "price": 656,
        "departure_country": "Sweden",
        "departure_region": "Dalarnas län",
        "destination_country": "Canada",
        "destination_region": "Yukon",
        "flight_number": "GN66852"
    },
    {
        "date": dt.datetime.fromisoformat("2022-12-14 22:12:00"),
        "destination_city": "Dublin",
        "departure_city": "Rotterdam",
        "price": 163,
        "departure_country": "Netherlands",
        "departure_region": "Zuid Holland",
        "destination_country": "Ireland",
        "destination_region": "Leinster",
        "flight_number": "FW87573"
    },
    {
        "date": dt.datetime.fromisoformat("2022-01-16 23:01:00"),
        "destination_city": "Doetinchem",
        "departure_city": "Guaitecas",
        "price": 708,
        "departure_country": "Chile",
        "departure_region": "Aisén",
        "destination_country": "Netherlands",
        "destination_region": "Gelderland",
        "flight_number": "XU37432"
    },
    {
        "date": dt.datetime.fromisoformat("2022-12-22 10:12:00"),
        "destination_city": "Ciudad Real",
        "departure_city": "Reyhanl",
        "price": 455,
        "departure_country": "Turkey",
        "departure_region": "Hatay",
        "destination_country": "Spain",
        "destination_region": "Castilla - La Mancha",
        "flight_number": "OH13761"
    },
    {
        "date": dt.datetime.fromisoformat("2023-05-09 08:05:00"),
        "destination_city": "Pazarcık",
        "departure_city": "Feira de Santana",
        "price": 305,
        "departure_country": "Brazil",
        "departure_region": "Bahia",
        "destination_country": "Turkey",
        "destination_region": "Kahramanmaraş",
        "flight_number": "SV27757"
    },
    {
        "date": dt.datetime.fromisoformat("2021-12-31 16:12:00"),
        "destination_city": "Vienna",
        "departure_city": "Codegua",
        "price": 68,
        "departure_country": "Chile",
        "departure_region": "O'Higgins",
        "destination_country": "Austria",
        "destination_region": "Vienna",
        "flight_number": "DM27429"
    },
    {
        "date": dt.datetime.fromisoformat("2022-05-29 21:05:00"),
        "destination_city": "Grado",
        "departure_city": "Nankana Sahib",
        "price": 377,
        "departure_country": "Pakistan",
        "departure_region": "Punjab",
        "destination_country": "Italy",
        "destination_region": "Friuli-Venezia Giulia",
        "flight_number": "FA58698"
    },
    {
        "date": dt.datetime.fromisoformat("2022-04-03 14:04:00"),
        "destination_city": "Rodengo/Rodeneck",
        "departure_city": "Buguma",
        "price": 128,
        "departure_country": "Nigeria",
        "departure_region": "Rivers",
        "destination_country": "Italy",
        "destination_region": "Trentino-Alto Adige",
        "flight_number": "XL92488"
    },
    {
        "date": dt.datetime.fromisoformat("2020-12-21 13:12:00"),
        "destination_city": "Padang Sidempuan",
        "departure_city": "Rạch Giá",
        "price": 429,
        "departure_country": "Vietnam",
        "departure_region": "Kiên Giang",
        "destination_country": "Indonesia",
        "destination_region": "North Sumatra",
        "flight_number": "HO63526"
    },
    {
        "date": dt.datetime.fromisoformat("2023-03-14 11:03:00"),
        "destination_city": "Diyarbakır",
        "departure_city": "Vĩnh Yên",
        "price": 913,
        "departure_country": "Vietnam",
        "departure_region": "Vĩnh Phúc",
        "destination_country": "Turkey",
        "destination_region": "Diyarbakır",
        "flight_number": "AD74564"
    },
    {
        "date": dt.datetime.fromisoformat("2022-06-14 07:06:00"),
        "destination_city": "Ferness",
        "departure_city": "Saalfelden am Steinernen Meer",
        "price": 274,
        "departure_country": "Austria",
        "departure_region": "Salzburg",
        "destination_country": "United Kingdom",
        "destination_region": "Nairnshire",
        "flight_number": "CP62878"
    },
    {
        "date": dt.datetime.fromisoformat("2023-10-18 16:10:00"),
        "destination_city": "Castri di Lecce",
        "departure_city": "Cerreto di Spoleto",
        "price": 542,
        "departure_country": "Italy",
        "departure_region": "Umbria",
        "destination_country": "Italy",
        "destination_region": "Puglia",
        "flight_number": "OY47582"
    },
    {
        "date": dt.datetime.fromisoformat("2022-07-11 11:07:00"),
        "destination_city": "Muradiye",
        "departure_city": "Bajaur Agency",
        "price": 102,
        "departure_country": "Pakistan",
        "departure_region": "FATA",
        "destination_country": "Turkey",
        "destination_region": "Van",
        "flight_number": "TG79251"
    },
    {
        "date": dt.datetime.fromisoformat("2022-01-29 16:01:00"),
        "destination_city": "Hilversum",
        "departure_city": "Gore",
        "price": 979,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "Netherlands",
        "destination_region": "Noord Holland",
        "flight_number": "ME37144"
    },
    {
        "date": dt.datetime.fromisoformat("2023-06-22 01:06:00"),
        "destination_city": "Borås",
        "departure_city": "Dunedin",
        "price": 33,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "Sweden",
        "destination_region": "Västra Götalands län",
        "flight_number": "FJ54557"
    },
    {
        "date": dt.datetime.fromisoformat("2023-02-02 08:02:00"),
        "destination_city": "Cork",
        "departure_city": "Tarnów",
        "price": 677,
        "departure_country": "Poland",
        "departure_region": "Małopolskie",
        "destination_country": "Ireland",
        "destination_region": "Munster",
        "flight_number": "ZY32235"
    },
    {
        "date": dt.datetime.fromisoformat("2022-10-29 01:10:00"),
        "destination_city": "Mercedes",
        "departure_city": "Paredones",
        "price": 818,
        "departure_country": "Chile",
        "departure_region": "O'Higgins",
        "destination_country": "Costa Rica",
        "destination_region": "Heredia",
        "flight_number": "XB34859"
    },
    {
        "date": dt.datetime.fromisoformat("2021-12-20 09:12:00"),
        "destination_city": "Salisbury",
        "departure_city": "Timaru",
        "price": 627,
        "departure_country": "New Zealand",
        "departure_region": "South Island",
        "destination_country": "United Kingdom",
        "destination_region": "Wiltshire",
        "flight_number": "OF91794"
    },
    {
        "date": dt.datetime.fromisoformat("2022-08-02 16:08:00"),
        "destination_city": "Edremit",
        "departure_city": "Cuttack",
        "price": 257,
        "departure_country": "India",
        "departure_region": "Odisha",
        "destination_country": "Turkey",
        "destination_region": "Balıkesir",
        "flight_number": "EO22115"
    },
    {
        "date": dt.datetime.fromisoformat("2022-12-04 19:12:00"),
        "destination_city": "Rödermark",
        "departure_city": "Gougnies",
        "price": 27,
        "departure_country": "Belgium",
        "departure_region": "Henegouwen",
        "destination_country": "Germany",
        "destination_region": "Hessen",
        "flight_number": "UB64225"
    },
    {
        "date": dt.datetime.fromisoformat("2021-09-25 10:09:00"),
        "destination_city": "Kaliningrad",
        "departure_city": "Topeka",
        "price": 304,
        "departure_country": "United States",
        "departure_region": "Kansas",
        "destination_country": "Russian Federation",
        "destination_region": "Kaliningrad Oblast",
        "flight_number": "JZ15393"
    },
    {
        "date": dt.datetime.fromisoformat("2022-07-22 06:07:00"),
        "destination_city": "Abingdon",
        "departure_city": "Hulst",
        "price": 986,
        "departure_country": "Netherlands",
        "departure_region": "Zeeland",
        "destination_country": "United Kingdom",
        "destination_region": "Berkshire",
        "flight_number": "JB78425"
    },
    {
        "date": dt.datetime.fromisoformat("2022-03-06 05:03:00"),
        "destination_city": "St. Johann in Tirol",
        "departure_city": "Hudiksvall",
        "price": 403,
        "departure_country": "Sweden",
        "departure_region": "Gävleborgs län",
        "destination_country": "Austria",
        "destination_region": "Tyrol",
        "flight_number": "HS29763"
    },
    {
        "date": dt.datetime.fromisoformat("2022-11-21 01:11:00"),
        "destination_city": "Haripur",
        "departure_city": "Quinchao",
        "price": 735,
        "departure_country": "Chile",
        "departure_region": "Los Lagos",
        "destination_country": "Pakistan",
        "destination_region": "Khyber Pakhtoonkhwa",
        "flight_number": "RI95676"
    },
    {
        "date": dt.datetime.fromisoformat("2022-05-19 11:05:00"),
        "destination_city": "Tame",
        "departure_city": "Monticelli d'Ongina",
        "price": 575,
        "departure_country": "Italy",
        "departure_region": "Emilia-Romagna",
        "destination_country": "Colombia",
        "destination_region": "Arauca",
        "flight_number": "VP95563"
    },
    {
        "date": dt.datetime.fromisoformat("2022-03-22 20:03:00"),
        "destination_city": "Koszalin",
        "departure_city": "Rueil-Malmaison",
        "price": 711,
        "departure_country": "France",
        "departure_region": "Île-de-France",
        "destination_country": "Poland",
        "destination_region": "Zachodniopomorskie",
        "flight_number": "YP29252"
    },
    {
        "date": dt.datetime.fromisoformat("2022-08-21 07:08:00"),
        "destination_city": "Buren",
        "departure_city": "Desamparados",
        "price": 340,
        "departure_country": "Costa Rica",
        "departure_region": "San José",
        "destination_country": "Netherlands",
        "destination_region": "Gelderland",
        "flight_number": "SY79418"
    },
    {
        "date": dt.datetime.fromisoformat("2023-02-26 16:02:00"),
        "destination_city": "Rangiora",
        "departure_city": "Buenaventura",
        "price": 667,
        "departure_country": "Colombia",
        "departure_region": "Valle del Cauca",
        "destination_country": "New Zealand",
        "destination_region": "South Island",
        "flight_number": "LU31835"
    },
    {
        "date": dt.datetime.fromisoformat("2021-10-09 03:10:00"),
        "destination_city": "Salihli",
        "departure_city": "Thái Bình",
        "price": 223,
        "departure_country": "Vietnam",
        "departure_region": "Thái Bình",
        "destination_country": "Turkey",
        "destination_region": "Manisa",
        "flight_number": "LO14179"
    },
    {
        "date": dt.datetime.fromisoformat("2023-03-10 23:03:00"),
        "destination_city": "Souvret",
        "departure_city": "Chicago",
        "price": 103,
        "departure_country": "United States",
        "departure_region": "Illinois",
        "destination_country": "Belgium",
        "destination_region": "Henegouwen",
        "flight_number": "RQ58273"
    },
    {
        "date": dt.datetime.fromisoformat("2023-09-12 23:09:00"),
        "destination_city": "Manavgat",
        "departure_city": "Tomaszów Mazowiecki",
        "price": 344,
        "departure_country": "Poland",
        "departure_region": "łódzkie",
        "destination_country": "Turkey",
        "destination_region": "Antalya",
        "flight_number": "JL18139"
    },
    {
        "date": dt.datetime.fromisoformat("2023-09-18 00:09:00"),
        "destination_city": "Evere",
        "departure_city": "Napier",
        "price": 104,
        "departure_country": "New Zealand",
        "departure_region": "North Island",
        "destination_country": "Belgium",
        "destination_region": "Brussels Hoofdstedelijk Gewest",
        "flight_number": "UX48478"
    },
    {
        "date": dt.datetime.fromisoformat("2021-08-29 12:08:00"),
        "destination_city": "Falun",
        "departure_city": "Bogotá",
        "price": 901,
        "departure_country": "Colombia",
        "departure_region": "Distrito Capital",
        "destination_country": "Sweden",
        "destination_region": "Dalarnas län",
        "flight_number": "EL25573"
    },
    {
        "date": dt.datetime.fromisoformat("2022-01-18 09:01:00"),
        "destination_city": "Glenrothes",
        "departure_city": "İmamoğlu",
        "price": 65,
        "departure_country": "Turkey",
        "departure_region": "Adana",
        "destination_country": "United Kingdom",
        "destination_region": "Fife",
        "flight_number": "DQ67617"
    },
    {
        "date": dt.datetime.fromisoformat("2021-10-07 15:10:00"),
        "destination_city": "Bauchi",
        "departure_city": "Cork",
        "price": 365,
        "departure_country": "Ireland",
        "departure_region": "Munster",
        "destination_country": "Nigeria",
        "destination_region": "Bauchi",
        "flight_number": "HW32814"
    },
    {
        "date": dt.datetime.fromisoformat("2022-07-11 22:07:00"),
        "destination_city": "Whitburn",
        "departure_city": "Juliaca",
        "price": 426,
        "departure_country": "Peru",
        "departure_region": "Puno",
        "destination_country": "United Kingdom",
        "destination_region": "West Lothian",
        "flight_number": "SF23563"
    },
    {
        "date": dt.datetime.fromisoformat("2022-04-20 11:04:00"),
        "destination_city": "Tczew",
        "departure_city": "Huntsville",
        "price": 703,
        "departure_country": "United States",
        "departure_region": "Alabama",
        "destination_country": "Poland",
        "destination_region": "Pomorskie",
        "flight_number": "OJ35555"
    },
    {
        "date": dt.datetime.fromisoformat("2021-07-25 08:07:00"),
        "destination_city": "Ely",
        "departure_city": "Huaraz",
        "price": 66,
        "departure_country": "Peru",
        "departure_region": "Ancash",
        "destination_country": "United Kingdom",
        "destination_region": "Cambridgeshire",
        "flight_number": "DZ66866"
    },
    {
        "date": dt.datetime.fromisoformat("2021-02-06 09:02:00"),
        "destination_city": "Feldkirch",
        "departure_city": "Edam",
        "price": 69,
        "departure_country": "Netherlands",
        "departure_region": "Noord Holland",
        "destination_country": "Austria",
        "destination_region": "Vorarlberg",
        "flight_number": "WK76475"
    },
    {
        "date": dt.datetime.fromisoformat("2021-07-27 06:07:00"),
        "destination_city": "Katowice",
        "departure_city": "Dillingen",
        "price": 399,
        "departure_country": "Germany",
        "departure_region": "Saarland",
        "destination_country": "Poland",
        "destination_region": "Sląskie",
        "flight_number": "TJ79746"
    },
    {
        "date": dt.datetime.fromisoformat("2022-04-26 01:04:00"),
        "destination_city": "North Waziristan",
        "departure_city": "Palu",
        "price": 877,
        "departure_country": "Indonesia",
        "departure_region": "Central Sulawesi",
        "destination_country": "Pakistan",
        "destination_region": "FATA",
        "flight_number": "LM54674"
    },
    {
        "date": dt.datetime.fromisoformat("2023-11-08 10:11:00"),
        "destination_city": "Gorzów Wielkopolski",
        "departure_city": "Isnes",
        "price": 500,
        "departure_country": "Belgium",
        "departure_region": "Namen",
        "destination_country": "Poland",
        "destination_region": "Lubuskie",
        "flight_number": "YL44157"
    },
    {
        "date": dt.datetime.fromisoformat("2021-05-17 12:05:00"),
        "destination_city": "Ełk",
        "departure_city": "Lagos",
        "price": 42,
        "departure_country": "Nigeria",
        "departure_region": "Lagos",
        "destination_country": "Poland",
        "destination_region": "Warmińsko-mazurskie",
        "flight_number": "DT39885"
    },
    {
        "date": dt.datetime.fromisoformat("2022-09-10 17:09:00"),
        "destination_city": "Bremerhaven",
        "departure_city": "San Rafael",
        "price": 308,
        "departure_country": "Costa Rica",
        "departure_region": "Alajuela",
        "destination_country": "Germany",
        "destination_region": "Bremen",
        "flight_number": "XK38387"
    },
    {
        "date": dt.datetime.fromisoformat("2021-03-02 05:03:00"),
        "destination_city": "Arequipa",
        "departure_city": "Kassel",
        "price": 829,
        "departure_country": "Germany",
        "departure_region": "Hessen",
        "destination_country": "Peru",
        "destination_region": "Arequipa",
        "flight_number": "LF42426"
    },
    {
        "date": dt.datetime.fromisoformat("2022-07-18 08:07:00"),
        "destination_city": "Acuña",
        "departure_city": "Chandigarh",
        "price": 215,
        "departure_country": "India",
        "departure_region": "Chandigarh",
        "destination_country": "Mexico",
        "destination_region": "Coahuila",
        "flight_number": "WE59281"
    },
    {
        "date": dt.datetime.fromisoformat("2022-12-23 15:12:00"),
        "destination_city": "Dangjin",
        "departure_city": "Liberia",
        "price": 150,
        "departure_country": "Costa Rica",
        "departure_region": "Guanacaste",
        "destination_country": "South Korea",
        "destination_region": "South Chungcheong",
        "flight_number": "WU73368"
    },
    {
        "date": dt.datetime.fromisoformat("2021-10-07 12:10:00"),
        "destination_city": "Stargard Szczeciński",
        "departure_city": "Kędzierzyn-Koźle",
        "price": 96,
        "departure_country": "Poland",
        "departure_region": "Opolskie",
        "destination_country": "Poland",
        "destination_region": "Zachodniopomorskie",
        "flight_number": "IA81225"
    }
]
