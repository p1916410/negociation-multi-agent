from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (

    QMainWindow

)

from src.ui.main_window import Ui_MainWindow


class Window(QMainWindow, Ui_MainWindow):

    def __init__(self,agents,parent=None):
        super().__init__(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setupUi(self,agents)
