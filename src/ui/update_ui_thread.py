import threading as th
from time import sleep


class UpdateUIThread(th.Thread):
    def __init__(self):
        super(UpdateUIThread, self).__init__()
        self.windows = []
        self.s = True

    def stop(self, delay=None):
        if delay:
            sleep(delay)
        self.s = False


    def run(self) -> None:
        while self.s:
            for win in self.windows:
                win.update_window()
            sleep(0.1)
