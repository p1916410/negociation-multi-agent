from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtGui import QColor

from src.world.agent import Buyer


class BuyerUI(QtWidgets.QWidget):
    def __init__(self, layout, buyer_agent: Buyer):
        super().__init__(layout)

        self.buyer_agent = buyer_agent

        self.name = buyer_agent.__repr__()

        self.max_price = buyer_agent.strategy.max_price
        self.max_date = buyer_agent.max_date
        self.destination = buyer_agent.dest_city

        self.color = buyer_agent.color

        self.setObjectName("buyer_agent")
        self.gridLayout_6 = QtWidgets.QGridLayout(self)
        self.gridLayout_6.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.label_max_price = QtWidgets.QLabel(self)
        self.label_max_price.setObjectName("max_price")
        self.gridLayout_6.addWidget(self.label_max_price, 4, 0, 1, 1)
        self.label_destination = QtWidgets.QLabel(self)
        self.label_destination.setObjectName("destination")
        self.gridLayout_6.addWidget(self.label_destination, 2, 0, 1, 1)
        self.name_buyer_agent = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(False)
        self.name_buyer_agent.setFont(font)
        self.name_buyer_agent.setObjectName("name_buyer_agent")

        name_color = QColor(self.color)
        name_color.setAlpha(50)

        self.name_buyer_agent.setStyleSheet(f'background-color: rgba{name_color.getRgb()};')
        self.gridLayout_6.addWidget(self.name_buyer_agent, 0, 0, 1, 1)
        self.constraints = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setUnderline(False)
        font.setWeight(75)
        self.constraints.setFont(font)
        self.constraints.setObjectName("constraints")
        self.gridLayout_6.addWidget(self.constraints, 1, 0, 1, 1)
        self.label_max_date = QtWidgets.QLabel(self)
        self.label_max_date.setObjectName("max_date")
        self.gridLayout_6.addWidget(self.label_max_date, 3, 0, 1, 1)
        self.label_status = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_status.setFont(font)
        self.label_status.setObjectName("status")
        self.gridLayout_6.addWidget(self.label_status, 5, 0, 1, 1)

    def retranslate_ui(self):
        _translate = QtCore.QCoreApplication.translate
        self.label_max_price.setText(_translate("MainWindow", f"Budget: {self.max_price}"))
        self.label_destination.setText(_translate("MainWindow", f"Destination: {self.destination}"))
        self.name_buyer_agent.setText(_translate("MainWindow", self.name))
        self.constraints.setText(_translate("MainWindow", "Constraints :"))
        self.label_max_date.setText(_translate("MainWindow", f"Date max: {self.max_date}"))
        self.label_status.setText(_translate("MainWindow", str(self.buyer_agent.status)))
