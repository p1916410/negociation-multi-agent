from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QTableWidgetItem

from src.world.message import MessageType


class MailboxUI(QtWidgets.QTableWidget):

    def __init__(self, layout):
        super().__init__(layout)
        self.setObjectName("table_mailbox")
        self.setColumnCount(4)
        self.setRowCount(0)
        header = self.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.setMinimumSize(QtCore.QSize(500, 0))
        item = QtWidgets.QTableWidgetItem()
        self.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.setHorizontalHeaderItem(3, item)

    def retranslate_ui(self):
        _translate = QtCore.QCoreApplication.translate
        item = self.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "FROM"))
        item = self.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "TO"))
        item = self.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "TYPE"))
        item = self.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "MESSAGE"))
        # TODO: Rendre les cases non modifiables

    def update_data(self, messages):
        _translate = QtCore.QCoreApplication.translate
        for message in messages:
            self.setRowCount(self.rowCount() + 1)
            self.setItem(self.rowCount() - 1, 0, QTableWidgetItem(str(message.sender)))

            self.setItem(self.rowCount() - 1, 1,
                         QTableWidgetItem(str(message.recipient)))

            self.setItem(self.rowCount() - 1, 2, QTableWidgetItem(message.message_type.value))
            if message.message_type == MessageType.DECLINE:
                self.setItem(self.rowCount() - 1, 3, QTableWidgetItem("Reason : " + message.reason))
            elif message.message_type != MessageType.ACCEPT:
                self.setItem(self.rowCount() - 1, 3, QTableWidgetItem(
                    str(message.ticket if message.ticket.flight_number is not None else " ") +
                    f" | Price proposed : {message.ticket.price if message.ticket.flight_number is not None else None}" +
                    f" | Destination : {message.ticket.destination_city}" if message.ticket else ""
                ))

            self.resizeColumnsToContents()
