from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtGui import QColor

from src.ui.ticket_ui import TicketUI
from src.world.agent import Seller


class SellerUI(QtWidgets.QWidget):

    def __init__(self, layout, seller:Seller):

        super().__init__(layout)
        self.name = seller.__repr__()
        self.company = seller.company.name
        self.tickets_list = seller.company.tickets

        self.color = seller.color



        self.setMinimumSize(QtCore.QSize(0, 300))
        self.setMaximumSize(QtCore.QSize(16777215, 400))
        self.setObjectName(self.name)
        self.gridLayout_14 = QtWidgets.QGridLayout(self)
        self.gridLayout_14.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout_14.setObjectName("gridLayout_" + self.name)
        self.scrollArea_tickets = QtWidgets.QScrollArea(self)
        self.scrollArea_tickets.setEnabled(True)
        self.scrollArea_tickets.setAutoFillBackground(False)
        self.scrollArea_tickets.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea_tickets.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scrollArea_tickets.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.scrollArea_tickets.setWidgetResizable(True)
        self.scrollArea_tickets.setObjectName("scrollArea_tickets")
        self.layout_tickets = QtWidgets.QWidget()
        self.layout_tickets.setGeometry(QtCore.QRect(0, 0, 353, 294))
        self.layout_tickets.setObjectName("layout_tickets")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.layout_tickets)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")

        self.tickets_UI_list = []
        for ticket in self.tickets_list:
            ticketUI = TicketUI(self.layout_tickets, ticket.flight_number, ticket.date, ticket.price,
                                ticket.departure_country, ticket.departure_region, ticket.departure_city,
                                ticket.destination_country, ticket.destination_region, ticket.destination_city)
            self.horizontalLayout_5.addWidget(ticketUI)
            self.sep_line_tickets = QtWidgets.QFrame(self.layout_tickets)
            self.sep_line_tickets.setFrameShape(QtWidgets.QFrame.VLine)
            self.sep_line_tickets.setFrameShadow(QtWidgets.QFrame.Sunken)
            self.sep_line_tickets.setObjectName("sep_line_tickets")
            self.horizontalLayout_5.addWidget(self.sep_line_tickets)
            self.tickets_UI_list.append(ticketUI)

        self.scrollArea_tickets.setWidget(self.layout_tickets)
        self.gridLayout_14.addWidget(self.scrollArea_tickets, 4, 0, 1, 1)
        self.tickets_assigned = QtWidgets.QLabel(self)
        self.tickets_assigned.setObjectName("tickets_assigned")
        self.gridLayout_14.addWidget(self.tickets_assigned, 3, 0, 1, 1)
        self.name_seller_agent = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.name_seller_agent.setFont(font)
        self.name_seller_agent.setObjectName("name_seller_agent")

        name_color = QColor(self.color)
        name_color.setAlpha(50)

        self.name_seller_agent.setStyleSheet(f'background-color: rgba{name_color.getRgb()};')


        self.gridLayout_14.addWidget(self.name_seller_agent, 1, 0, 1, 1)
        self.label_company = QtWidgets.QLabel(self)
        self.label_company.setObjectName("company")
        self.gridLayout_14.addWidget(self.label_company, 2, 0, 1, 1)

    def retranslate_ui(self):

        _translate = QtCore.QCoreApplication.translate
        for ticket in self.tickets_UI_list:
            ticket.retranslate_ui()
        self.tickets_assigned.setText(_translate("MainWindow", "Tickets assigned : "))
        self.name_seller_agent.setText(_translate("MainWindow", self.name))
        self.label_company.setText(_translate("MainWindow", self.company))
