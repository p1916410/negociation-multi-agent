from PyQt5 import QtWidgets, QtCore, QtGui


class TicketUI(QtWidgets.QWidget):
    def __init__(self, layout, flight_number, date, amount, departure_country, departure_region, departure_city,
                 destination_country, destination_region, destination_city):
        super().__init__(layout)
        self.flight_number = flight_number
        self.date = date
        self.amount = amount
        self.departure_country = departure_country
        self.departure_region = departure_region
        self.departure_city = departure_city
        self.destination_country = destination_country
        self.destination_region = destination_region
        self.destination_city = destination_city

        self.setMinimumSize(QtCore.QSize(0, 0))
        self.setMaximumSize(QtCore.QSize(16777215, 500))
        self.setObjectName("ticket")
        self.gridLayout_15 = QtWidgets.QGridLayout(self)
        self.gridLayout_15.setObjectName("gridLayout_15")
        self.label_departure_city = QtWidgets.QLabel(self)
        self.label_departure_city.setObjectName("departure_city")
        self.gridLayout_15.addWidget(self.label_departure_city, 8, 0, 1, 1)
        self.label_flight_number = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.label_flight_number.setFont(font)
        self.label_flight_number.setObjectName("flight_number")
        self.gridLayout_15.addWidget(self.label_flight_number, 5, 0, 1, 1)
        self.label_departure_country = QtWidgets.QLabel(self)
        self.label_departure_country.setObjectName("departure_country")
        self.gridLayout_15.addWidget(self.label_departure_country, 10, 0, 1, 1)
        self.label_destination_country = QtWidgets.QLabel(self)
        self.label_destination_country.setObjectName("destination_country")
        self.gridLayout_15.addWidget(self.label_destination_country, 10, 1, 1, 1)
        self.label_destination_city = QtWidgets.QLabel(self)
        self.label_destination_city.setObjectName("destination_city")
        self.gridLayout_15.addWidget(self.label_destination_city, 8, 1, 1, 1)
        self.label_departure_region = QtWidgets.QLabel(self)
        self.label_departure_region.setObjectName("departure_region")
        self.gridLayout_15.addWidget(self.label_departure_region, 9, 0, 1, 1)
        self.label_destination_region = QtWidgets.QLabel(self)
        self.label_destination_region.setObjectName("destination_region")
        self.gridLayout_15.addWidget(self.label_destination_region, 9, 1, 1, 1)
        self.label_destination = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_destination.setFont(font)
        self.label_destination.setObjectName("label_destination")
        self.gridLayout_15.addWidget(self.label_destination, 7, 1, 1, 1)
        self.label_departure = QtWidgets.QLabel(self)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_departure.setFont(font)
        self.label_departure.setObjectName("label_departure")
        self.gridLayout_15.addWidget(self.label_departure, 7, 0, 1, 1)
        self.label_amount = QtWidgets.QLabel(self)
        self.label_amount.setObjectName("amount")
        self.gridLayout_15.addWidget(self.label_amount, 6, 0, 1, 1)
        self.label_date = QtWidgets.QLabel(self)
        self.label_date.setObjectName("date")
        self.gridLayout_15.addWidget(self.label_date, 6, 1, 1, 1)

    def retranslate_ui(self):
        _translate = QtCore.QCoreApplication.translate
        self.label_departure_city.setText(_translate("MainWindow", self.departure_city))
        self.label_flight_number.setText(_translate("MainWindow", "Flight n°" + self.flight_number))
        self.label_departure_country.setText(_translate("MainWindow", self.departure_country))
        self.label_destination_country.setText(_translate("MainWindow", self.destination_country))
        self.label_destination_city.setText(_translate("MainWindow", self.destination_city))
        self.label_departure_region.setText(_translate("MainWindow", self.departure_region))
        self.label_destination_region.setText(_translate("MainWindow", self.destination_region))
        self.label_destination.setText(_translate("MainWindow", "Destination : "))
        self.label_departure.setText(_translate("MainWindow", "Departure : "))
        self.label_amount.setText(_translate("MainWindow", str(self.amount) + " €"))
        self.label_date.setText(_translate("MainWindow", str(self.date)))
