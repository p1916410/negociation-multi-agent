# Négociation Multi-Agent

Réalisé par Jeremy Kritikos et Tom Montauriol.

Le but de ce projet est de mettre en place une négociation entre différents agents pour le commerce de billets.

## Installation

Le projet utilise Python en version 3.9 ou supérieur.

Les dépendances sont :

* PyQt5

Pour les installer de manière automatique, il suffit de se placer à la racine du projet et d'éxécuter la commande :

```pip install -r requirements.txt```

Ensuite, le projet se lance comme un module Python (toujours depuis la racine du projet):

``python3 -m src``

## Utilisation

Pour modifier les agents Acheteurs, il faut modifier le fichier JSON `src/buyers.json`. C'est une liste d'acheteurs, il
est possible donc d'en rajouter ou d'en enlever.

Les vols et les compagnies sont modifiables dans le fichier Python `src/data/data.py`.

## Démonstration

Une vidéo de démonstration est disponible ici : https://youtu.be/bSkv1mqOx4Y
